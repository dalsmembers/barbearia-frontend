import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './stores/store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false

Vue.use(VueAxios,axios)

new Vue({
  render: h => h(App),
  router,
  vuetify,
  store
}).$mount('#app')
