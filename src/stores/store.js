/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		count: 0,
		user: {}
	},
	mutations: {
		logoff (state){
      		state.user = {}
    	},
		setUser (state, payload){
			state.user = payload;
		}
	},
	getters: {
		getUser: (state) => {
			return state.user
		}
	}
})

export default store;